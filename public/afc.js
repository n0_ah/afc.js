// Gets all the nessesary Elements, or creates the variables for them
var ascii = document.querySelectorAll('.ascii');

// Loops over all .ascii elements on the page
ascii.forEach(function(ascii) {
  var asciiLines = ascii.querySelectorAll('pre');
  var colorSpans;
  var configs = [];

  // Stores the attributes/settings
  var asciiColorType = ascii.getAttribute('ascii_color_type');
  // Should be a Number, leave it empty for a Strobo-Effect
  var asciiInterval = Number(ascii.getAttribute('ascii_interval'));
  var hasAsciiInterval = ascii.hasAttribute('ascii_interval');
  // Modes
  var asciiMode = ascii.getAttribute('ascii_mode');
  var hasAsciiMode = ascii.hasAttribute('ascii_mode');

  // Get the Config from the ascii_characters attripute of the .ascii element
  var asciiCharacters = ascii.getAttribute('ascii_characters').split(',');

  // Generates a random color based on the asciiColorType attribute
  function randomColor() {
    // Generates a color array
    function colorGen(value1, value2, value3) {
      return [
        Math.floor(Math.random() * value1),
        Math.floor(Math.random() * value2),
        Math.floor(Math.random() * value3)
      ];
    }

    // HSL
    if (asciiColorType === 'hsl') {
      return colorGen(360,100,100);

    // default RGB
    } else {
      return colorGen(255,255,255);
    }
  }

  // Calculates the Color steps between the start and end colors, depending on the available lines
  function calculateSteps(config) {
    config.colorSteps = [];

    config.endColor.forEach(function(color, colorIndex) {
      config.colorSteps.push(Math.floor((color - config.startColor[colorIndex]) / asciiLines.length));
    });
  }

  // Sets the current Color, is run for each span
  function setCurrentColor(line, lineIndex, config, configIndex) {
    if(lineIndex === 0) {
      config.currentColor = config.startColor;

    } else if(lineIndex + 1 === asciiLines.length) {
      config.currentColor = config.endColor;

    } else {
      config.currentColor.forEach(function(color, colorIndex) {
        config.currentColor[colorIndex] = color + config.colorSteps[colorIndex];
      });
    }
  }

  // Loops over every character and creates the classes for the different caracters.
  function characterCrawler(lineArray) {
    var lastCharacter = '';

    lineArray.forEach(function (character, characterIndex) {
      var beginning = '';
      var closure = '';

      configs.forEach(function(config, configIndex) {
        if (config.characters.includes(lastCharacter) && !config.characters.includes(character)) {
          closure += '</span>';

        } else if (!config.characters.includes(lastCharacter) && config.characters.includes(character)) {
          beginning += '<span class="ascii' + configIndex + '">';
        }
      });

      lineArray[characterIndex] = closure + beginning + character;
      lastCharacter = character;
    });
  }

  // Loops over every line to create the class
  function lineCrawler() {
    asciiLines.forEach(function (line, lineIndex) {
      var lineArray = line.innerHTML.split('');

      characterCrawler(lineArray);

      line.innerHTML = lineArray.join('');
    });
  }

  // Renders the color for each line, depending of the character type and color grade(step)
  function setColor(asciiLines, asciiColorType) {
    configs.forEach(function(config, configIndex) {

      calculateSteps(config);

      asciiLines.forEach(function (line, lineIndex) {
        setCurrentColor(line, lineIndex, config, configIndex);

        var asciiSelection = line.querySelectorAll('.ascii' + configIndex);

        asciiSelection.forEach(function(ascii) {
          function hsl() {
            var newCurrentColor = [];
            config.currentColor.forEach(function(element, elementID) {
              if (elementID === 0) {
                newCurrentColor.push(element);

              } else {
                newCurrentColor.push(element + '%');
              }
            });
            return newCurrentColor;
          }
          // HSL
          if (asciiColorType === 'hsl') {
            ascii.style.color = 'hsl' + '(' + hsl() + ')';

          // default RGB, also in case of an error...
          } else {
            ascii.style.color = 'rgb' + '(' + config.currentColor + ')';
          }
        });
      });
    });
  }

  // Initial definition of the characters, with start and end color
  asciiCharacters.forEach(function(config) {
    configs.push({
      characters: config.split(''),
      startColor: randomColor(),
      endColor: randomColor()
    });
  });

  // Injects the spans/classess for the different characters
  lineCrawler();

  // Stores all span elements that have been created by this script
  colorSpans = ascii.querySelectorAll('span');

  // Sets the css transition-speed according to the ascii_interval attribute
  function setTransition() {
    colorSpans.forEach(function(span) {
      span.style.transition = 'color ' + asciiInterval + 's linear';
    });
  }

  // Modes - Rainbow
  if (hasAsciiMode && asciiMode === 'rainbow') {
    asciiColorType = 'hsl';
    var degree = 0;

    // Sets the color at every browser render frame
    function rainbow() {
      configs.forEach(function(config, configID) {
        config.startColor = [degree,65,65];
        config.endColor = [degree,35,35];
      });

      setColor(asciiLines, asciiColorType);

      // Counts to 360, then sets it to 0 again...
      if (degree < 360) {
        degree++;
      } else {
        degree = 0;
      }
      requestAnimationFrame(rainbow);
    };
    requestAnimationFrame(rainbow);

  // Modes - Waves
  } else if (hasAsciiMode && asciiMode === 'waves') {
    asciiColorType = 'hsl';
    var degree = 0;

    // Sets the color at every browser render frame
    function waves() {
      configs.forEach(function(config, configID) {
        config.startColor = [degree,50,50];
        config.endColor = [(degree + 60),50,50];
      });

      setColor(asciiLines, asciiColorType);

      // Counts to 360, then subtracts 360, for the endColor with the + 60
      if (degree < 360) {
        degree++;
      } else {
        degree += -360;
      }
      requestAnimationFrame(waves);
    };
    requestAnimationFrame(waves);

  // If the asciiInterval is defined, set an interval
  } else if (asciiInterval > 0 || hasAsciiInterval) {
    setColor(asciiLines);
    setTransition();

    setInterval(function() {
      configs.forEach(function(config, configID) {
        config.startColor = randomColor();
        config.endColor = randomColor();
      });

      setColor(asciiLines, asciiColorType);
    }, asciiInterval * 1000);}

    // Sets the color initiali, is necessary for the case, where you don't want a random color
    else {
      setColor(asciiLines, asciiColorType);
    }
});